# MongoDB Installation

**Requirements**

* Installed kubectl command-line tool
* Connected to a Kubernetes cluster - Have a kubeconfig file (default location is /kube/config)

**MongoDB Installation**

1. Create mongodb.yaml file with definition of pod and service of mongodb
2. Write the command:
```
 kubectl apply -f mongodb.yaml
 ```
   to deploy the pod and the service of mongodb
